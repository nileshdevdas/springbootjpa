package com.db.microservices;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * only INterface
 * 
 * @author niles
 *
 */
public interface UserDAO extends JpaRepository<User, Long> {

}
